<?php

namespace MiamiOH\PhpLmsService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class LmsResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(

            'name' => 'Lms',

            'class' => 'MiamiOH\PhpLmsService\Services\Lms',

            'description' => 'Information about lms',

            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),

        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(

            'action' => 'read',

            'name' => 'academic.lms.all',

            'description' => 'Read a list of all courses',

            'pattern' => '/academic/lms',

            'service' => 'Lms',

            'method' => 'getCanvasCourse',

            'middleware' => array(),

            'returnType' => 'collection',

        ));

        $this->addResource(array(

            'action' => 'read',

            'name' => 'lms.section',

            'description' => 'Read a list of courses by term',

            'pattern' => '/academic/lms/section',

            'service' => 'Lms',

            'method' => 'getCanvasCourse',

            'returnType' => 'collection',

            'options' => array(
                'termcrn' => array('type' => 'list', 'description' => 'term and crn pair'),
            ),

        ));

        $this->addResource(array(

            'action' => 'read',

            'name' => 'lms.section.termcrn',

            'description' => 'Read a list of courses by term',

            'pattern' => '/academic/lms/section/:termcrn',

            'service' => 'Lms',

            'method' => 'getCanvasCourse',

            'returnType' => 'model',

            'params' => array(
                'termcrn' => array('description' => 'Term CRN Pair'),
            ),

            'middleware' => array(),

        ));

    }

    public function registerOrmConnections(): void
    {

    }
}