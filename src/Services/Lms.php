<?php
/*
-----------------------------------------------------------
FILE NAME: Lms.class.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Jimmy Xu

DESCRIPTION:  the implementation of the lmsSite api which will be part of the new REST framework.
The resource we create will take parameters and fetch data about an lms site (via an lms’s web service)
to return to the requestor.  It is intended to be lms-generic, but for the first implementation,
we will be connecting to Canvas.
The real purpose of this api is to bridge a gap caused by the inability for a production VLAN server to
gain access to the internet to call Canvas.  This PHP web service will be an appropriate stepping stone,
or proxy, so that the server can get its data from Canvas without gaining access to the internet.


INPUT:
PARAMETERS: termcrn

ENVIRONMENT DEPENDENCIES: REST FRAMEWORK

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

01/01/2015               XUJ4
Description:  Initial Program
05/07/2015               XUJ4
Description:
1. Change the service call from api to local table for performance purpose
2. Add date check and restriction check to determine availability of a course
3. change name space to Service\Academic\LMS\Lms
4.

08/25/2015				MILLSE
Description:  Date check bug fix
The code was trying to check an ISO formatted PHP date object against the raw date text
from the LMS databsae.  To correct this, I made both the current date and database date
into PHP date objects before checking them against each other.  Also, the current date 
is in GMT because that is what the LMS database is holding the dates as.

09/21/2015				SCHMIDEE
Corrected SQL Injection Problem.
 */

namespace MiamiOH\PhpLmsService\Services;

class Lms extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $whereClause;

    //	Helper functions that were called by the frame work and create internal datasource and configuration objects
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    //Start the main function
    public function getCanvasCourse()
    {

        //build debug
        $this->log->debug('Start the getCanvasCourse service.' . date(DATE_ISO8601, time()));
        date_default_timezone_set('EST');

        //set rest variables
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();
        $termcrn_arr = array();

        //get parameters from user input (url)
        if (isset($options['termcrn']) && is_array($options['termcrn'])) {
            foreach ($options['termcrn'] as $onetermcrn) {
                $termcrn_arr[] = $onetermcrn;
            }
        } else if ($request->getResourceParam('termcrn')) {
            $termcrn_arr = explode(',', $request->getResourceParam('termcrn'));
        } else {
            throw new \Exception('Error getting options or parameter: No Termcrn is available');
        }

        try {
            //get Configuration value
            $lmstranslation = $this->configuration->getConfiguration('LmsService', 'Translation');//pull all the translation values from LmsService/Config in ConfigMgr

            if (empty($lmstranslation)) {
                throw new \Exception('Error getting configure manager for translation information');
            }

        } catch (\Exception $e) {
            throw new \Exception('Error getting value from configure manager' . $e);
        }

        $records = $this->courseSectionData($termcrn_arr);
        $payload = $this->checkAvailability($records, $lmstranslation, $termcrn_arr);

        if ($payload) {
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);
        } else {
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            $response->setPayload($payload);
            $this->log->info("Error: Payload was not set successfully ");

        }

        $this->log->debug("Call ended.");
        return $response;

    }

    private function checkAvailability($records, $lmstranslation, $termcrn_arr)
    {
        $record = array();
        //Loop through all input term crn combo
        foreach ($termcrn_arr as $termcrn) {

            $is_published = "false";
            $isFound = "false";
            $course_id = "";
            $workflow_state = "";

            //loop through all return records from database
            foreach ($records as $onecourse) {

                //if the term crn combo can not be found from database query, means there is no course
                //availbe in CANVAS table
                if (strcmp($onecourse['term'] . $onecourse['crn'], $termcrn) !== 0) {
                    //TODO: if term crn not found, do nothing
                } //course if found in database query return records
                else {
                    //Set course found to ture if the course is in the table
                    $isFound = "true";

                    $start_at = date($onecourse['section_start']);
                    $end_at = date($onecourse['section_end']);
                    $workflow_state = strtolower($onecourse['course_workflow']);
                    $course_id = strtolower($onecourse['canvas_site_id']);
                    $restricted = strtolower($onecourse['course_restrict_past']);

                    $currentDate = gmdate('Y-m-d H:i:s', time()); //changed from ISO EST time to formatted GMT time to match lms database. -millse 08/25/2015
                    //echo "restricted".$restricted."check";

                    //if the start date is null or early than sysdate and end date
                    //is null or later than sysdate, then the corse may be available, check workflow
                    if ($start_at == null || $start_at == 'null' || $currentDate >= $start_at) {
                        if ($restricted == 'false' || $restricted == 0) {//when there is no restriction updated 5/20
                            $is_published = $this->checkWorkFlow($workflow_state, $lmstranslation);
                        } else {
                            if ($end_at == null || $end_at == 'null' || $currentDate <= $end_at) {
                                $is_published = $this->checkWorkFlow($workflow_state, $lmstranslation);
                            }
                        }
                    }
                }
            }

            array_push($record, array(
                'termcrn' => $termcrn,
                'courseId' => $course_id,
                'workflow_state' => $workflow_state,
                'is_published' => $is_published,
                'course_found' => $isFound,
            ));

        }

        return $record;
    }

    //This function takes workflow state string, check against configuration value
    //and return result (a translation process)
    private function checkWorkFlow($workflow_state, $lmstranslation)
    {
        //workflow check, if workflow is in available. Return value from configmanager
        if ($workflow_state) {

            $workflow_state = str_replace("'", "", $workflow_state);

            foreach ($lmstranslation as $key => $value) {
                if ("workflow_state." . $workflow_state == $key) {
                    return $value;
                    break;
                }
            }
        }

        return "false";

    }

    private function courseSectionData($termcrn_array)
    {
        $results = array();
        $orString = "";
        $whereClause = "";

        foreach ($termcrn_array as $termcrn) {
            if (is_numeric($termcrn)) {
                //Only Allow Numeric Information (Prevents SQL Injection Attacks)
                $whereClause .= $orString . "(term = '" . substr($termcrn, 0, 6) . "' and crn ='" . substr($termcrn, 6, 13) . "')";
                $orString = " OR ";
            }
        }

        try {

            $datasource_name = 'LMSMGR';
            $dbh = $this->database->getHandle($datasource_name);
            $queryString = "SELECT 
									term,
									crn,
									section_name,
									canvas_site_id, 
									canvas_site_name, 
									course_workflow, 
									course_restrict_past, 
									course_restrict_future,
									TO_CHAR( section_start, 'YYYY-MM-DD HH24:MI:SS' ) as section_start,
									TO_CHAR( section_end, 'YYYY-MM-DD HH24:MI:SS' ) as section_end
								FROM canvas.sections_info where " . $whereClause;
            $results = $dbh->queryall_array($queryString);

        } catch (\Exception $e) {
            throw new \Exception('Error getting data: ' . $e);
        }

        return $results;
    }

}
