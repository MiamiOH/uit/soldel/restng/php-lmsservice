<?php

namespace MiamiOH\PhpLmsService\Tests\Unit;

class LmsTest extends \MiamiOH\RESTng\Testing\TestCase
{

    //////////
    //Set Up//
    //////////
    private $dbh, $lms, $queryallRecords;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock api:
        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam'))
            ->getMock();

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'returnResourceParam')));

        $request->method('getOptions')
            ->will($this->returnCallback(array($this, 'returnOptions')));

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->lms = new \MiamiOH\PhpLmsService\Services\Lms();
        $this->lms->setLogger();
        $this->lms->setDatabase($db);
        $this->lms->setDatasource($ds);
        $this->lms->setRequest($request);

    }

    public function testGetCanvasCourse()
    {
        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'queryall_arraySectionsInfo')));

        //get the response and payload from the getSchedule() method.
        $resp = $this->lms->getCanvasCourse();
        $payload = $resp->getPayload();

        $this->assertEquals($payload[0]['section_start'], $this->queryallRecords[0]['section_start']);

        $this->assertEquals("1", "1");

    }

    public function returnOptions()
    {
        $optionsArray = array('termcrn' => array('201420', '76516', '201310','10101',),);
        return $optionsArray;
    }

    public function queryall_arraySectionsInfo()
    {
        $this->queryallRecords = array(
            array(
                'term' => '201510',
                'crn' => '18173',
                'section_start' => 'null',
                'section_end' => 'null',
                'course_workflow' => 'unpublished',
            ),

            array(
                'term' => '201530',
                'crn' => '33639',
                'section_start' => '2011-11-11',
                'section_end' => '2015-07-04',
                'course_workflow' => 'unpublished',
            ),

        );

        return $this->queryallRecords;
    }

}
